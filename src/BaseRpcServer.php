<?php
namespace Imi\Rpc;

use Imi\Server\Base;
use Imi\Rpc\Contract\IRpcServer;

/**
 * RPC 服务器基类
 */
abstract class BaseRpcServer extends Base implements IRpcServer
{

}
